using NUnit.Framework;

namespace Laba3TRPO.Nunit
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            double s;
            s=ClassLibary.Class1.Func(3, 5);
            Assert.AreEqual(15.484, s,0.001);
        }
    }
}