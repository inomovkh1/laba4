﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Laba4TRPO.wpf
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (a.Text != "" && n.Text != "")
            {
                double a1 = Convert.ToDouble(a.Text);
                double n1 = Convert.ToDouble(n.Text);
                double s = Laba3TRPO.ClassLibary.Class1.Func(a1, n1);
                label1.Content = s;
            }
            else
            { 
                label1.Content = "";
            }
        }
    }
}
