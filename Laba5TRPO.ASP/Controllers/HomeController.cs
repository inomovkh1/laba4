﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Laba3TRPO.ClassLibary;

namespace TRPO_ASP.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Index(string a, string n)
        {
            double s = Class1.Func(Convert.ToDouble(a), Convert.ToDouble(n));
            ViewBag.result = s;
            return View();
        }


    }
}